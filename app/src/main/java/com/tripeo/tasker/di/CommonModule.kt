package com.tripeo.tasker.di

import android.content.Context
import android.content.SharedPreferences
import com.tripeo.tasker.auth.AuthRepository
import com.tripeo.tasker.auth.AuthRepositoryImpl
import com.tripeo.tasker.ui.naviagation.Navigator
import com.tripeo.tasker.ui.naviagation.NavigatorImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object CommonModule {

    @Singleton
    @Provides
    fun provideSharedPreferences(@ApplicationContext context: Context): SharedPreferences {
        return context.getSharedPreferences(COMMON_PREFS, Context.MODE_PRIVATE)
    }

    @Singleton
    @Provides
    fun provideAuthRepository(preferences: SharedPreferences): AuthRepository {
        return AuthRepositoryImpl(preferences)
    }

    @Singleton
    @Provides
    fun provideNavigator(): Navigator {
        return NavigatorImpl()
    }

    private const val COMMON_PREFS = "common_preferences"
}