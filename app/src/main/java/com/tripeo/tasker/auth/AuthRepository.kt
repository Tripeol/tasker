package com.tripeo.tasker.auth

import android.content.SharedPreferences
import com.google.firebase.auth.FirebaseAuth
import javax.inject.Inject

interface AuthRepository {

    /**
     * Return userId from cache or from Firebase if cache is empty
     */
    @Throws(AuthentificateException::class)
    fun receiveUserId(): String
}

class AuthRepositoryImpl @Inject constructor(val preferences: SharedPreferences) : AuthRepository {
    override fun receiveUserId(): String {
        val cachedValue = preferences.getString(USER_ID_KEY, null)
        if (cachedValue == null) {
            val userId = FirebaseAuth.getInstance().currentUser?.uid
            if (userId != null) {
                preferences.edit().putString(USER_ID_KEY, userId).apply()
                return userId
            } else {
                throw AuthentificateException()
            }
        } else {
            return cachedValue
        }

    }

    companion object {
        private const val USER_ID_KEY = "uID"
    }

}