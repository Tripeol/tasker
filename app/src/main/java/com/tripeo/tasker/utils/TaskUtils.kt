package com.tripeo.tasker.utils

import com.tripeo.tasker.data.models.Task
import com.tripeo.tasker.data.models.TaskState

fun getFinishedSubtasksAmount(task: Task): Int {
    var result = 0;

    var subtasks = task.subtasks
    if (subtasks != null) {
        for (subtask in subtasks) {
            if (subtask.state != null && subtask.state == TaskState.DONE) {
                result++
            }
        }
    }

    return result
}
