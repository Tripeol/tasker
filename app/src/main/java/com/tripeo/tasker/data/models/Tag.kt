package com.tripeo.tasker.data.models

/**
 * Класс описывающий тэг задач
 *
 * @property id идентификатор тэга (nullable так как мы незнаем индентификатор при создании нового тэга)
 * @property name текст отображаемый на тэге
 *
 * @author Trikhin P O
 * @since 08 11 2020
 */
data class Tag(
    val id: String?,
    val name: String
)