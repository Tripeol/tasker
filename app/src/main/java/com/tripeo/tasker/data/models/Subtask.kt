package com.tripeo.tasker.data.models

import java.io.Serializable

/**
 * Сущность подзадачи внутри [Task]
 *
 * @property id идентификатор подзадачи
 * @property body тело подзадачи
 * @property state состояние подзадачи
 * @property weight вес подзадачи, в сравнении с другими подзадачами.
 */
data class Subtask constructor(
    var id: String?,
    var body: String?,
    var state: TaskState?,
    var weight: Int? = 0
) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Subtask

        if (id != other.id) return false
        if (body != other.body) return false
        if (state != other.state) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (body?.hashCode() ?: 0)
        result = 31 * result + (state?.hashCode() ?: 0)
        return result
    }
}