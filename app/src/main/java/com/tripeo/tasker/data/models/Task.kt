package com.tripeo.tasker.data.models

import java.io.Serializable

/**
 * Модель задачи
 *
 * @property body текст задачи
 * @property state состояние задачи
 * @property subtasks список подзадач
 * @property weight вес задачи
 * @property tags список тэгов данной задачи
 *
 * @author Trikhin P O
 * @since 11.11.2019
 */
data class Task constructor(
    var body: String? = null,
    var state: TaskState? = null,
    var subtasks: MutableList<Subtask>? = null,
    var weight: Int? = null,
    var tags: List<Tag>? = null
) : Serializable {

    /**
     * @param body текст задачи
     * @param state состояние задачи
     * @param id идентификатор задачи, null если задача новая и идентификатора все еще нет
     * @param subtasks список подзадач
     * @param weight вес задачи
     * @param tags список тэгов данной задачи
     */
    constructor(
        body: String?,
        state: TaskState?,
        id: String?,
        subtasks: MutableList<Subtask>?,
        weight: Int?,
        tags: List<Tag>?
    ) : this(body, state, subtasks, weight, tags) {
        this.id = id
    }

    var id: String? = null
}