package com.tripeo.tasker.data.models

/**
 * Состояине задачи
 */
enum class TaskState constructor(value: String) {

    /**
     * Задача в изначальном состоянии
     */
    TODO("todo"),

    /**
     * Задача в завершенном состоянии
     */
    DONE("done")
}