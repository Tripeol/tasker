package com.tripeo.tasker

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TaskerApplication: Application() {
}