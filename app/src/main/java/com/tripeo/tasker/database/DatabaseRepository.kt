package com.tripeo.tasker.database

import com.google.firebase.database.DataSnapshot
import com.tripeo.tasker.data.models.Tag
import com.tripeo.tasker.data.models.Task
import com.tripeo.tasker.database.dao.TagDAO
import com.tripeo.tasker.database.dao.TaskDAO
import com.tripeo.tasker.database.dao.writers.TagWriter
import com.tripeo.tasker.database.dao.writers.TaskWriter
import javax.inject.Inject

/**
 * Класс для общения с базой данных
 */
class DatabaseRepository @Inject constructor(val databaseHelper: DatabaseHelper) {

    private var taskDAO: TaskDAO
    private var tagDAO: TagDAO

    init {
        taskDAO = TaskDAO()
        tagDAO = TagDAO()
    }

    /**
     * Записать таску [Task] в БД
     *
     * @param taskWriter [TaskWriter] содержащий информацию о [Task]
     */
    fun writeTaskWithWriter(taskWriter: TaskWriter) {
        taskDAO.writeTask(databaseHelper.getTasksRootReference(), taskWriter)
    }

    /**
     * Получить список тасок
     *
     * @param dataSnapshot указатель на корень БД где лежат таски users/<id юзера>/tasks/
     */
    fun getTasks(dataSnapshot: DataSnapshot): ArrayList<Task> {
        val tasks = ArrayList<Task>()
        for (child in dataSnapshot.children) {
            val task = taskDAO.getTask(child)
            if (task != null) {
                tasks.add(task)
            }
        }
        return tasks
    }

    /**
     * Получить таску
     *
     * @param dataSnapshot указатель на корень БД где лежат таски users/<id юзера>/tasks/<id таски>
     */
    fun getTask(dataSnapshot: DataSnapshot): Task? {
        return taskDAO.getTask(dataSnapshot)
    }

    /**
     * Записать новый или измененный тэг в БД
     *
     * @param tagWriter вспомогательный класс подготавливающий данный для записи [Tag]]
     */
    fun writeTagWithWriter(tagWriter: TagWriter) {
        tagDAO.writeTag(databaseHelper.getTagsRootReference(), tagWriter)
    }

    /**
     * Получить список тэгов
     *
     * @param dataSnapshot указатель на корень БД где лежат таски users/<id юзера>/tags
     */
    fun getTags(dataSnapshot: DataSnapshot): List<Tag> {
        val tags = ArrayList<Tag>()
        for (child in dataSnapshot.children) {
            val tag = tagDAO.getTag(child)
            if (tag != null) {
                tags.add(tag)
            }
        }
        return tags
    }
}