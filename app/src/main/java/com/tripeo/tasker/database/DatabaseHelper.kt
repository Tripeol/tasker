package com.tripeo.tasker.database

import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.tripeo.tasker.auth.AuthRepository
import com.tripeo.tasker.auth.AuthentificateException
import javax.inject.Inject

/**
 * Вспомогательный класс для работы с базой
 */
class DatabaseHelper @Inject constructor(
    val authRepository: AuthRepository
) {

    private val fireBaseDatabase: FirebaseDatabase
    private val USERS = "users"
    private val TASK = "tasks"
    private val TAGS = "tags"
    private val SEPARATOR = "/"

    init {
        fireBaseDatabase = FirebaseDatabase.getInstance()
    }

    /**
     * Получить инстанс базы данных firebase
     */
    fun getDatabase(): FirebaseDatabase {
        return fireBaseDatabase;
    }

    /**
     * Получить [DatabaseReference] указывающую на путь в базе данных к списку задач users/sdfhakj/tasks
     *
     * Выбрасывает исключение [AuthentificateException] если пользователь не удалось получить этот путь
     */
    @Throws(AuthentificateException::class)
    fun getTasksRootReference(): DatabaseReference {
        return getDatabase().getReference(getTasksRootPath())
    }

    /**
     * Получить [DatabaseReference] указывающую на путь в базе данных к списку тэгов users/sdfhakj/tags
     *
     * Выбрасывает исключение [AuthentificateException] если пользователь не удалось получить этот путь
     */
    @Throws(AuthentificateException::class)
    fun getTagsRootReference(): DatabaseReference {
        return getDatabase().getReference(getTagsRootPath())
    }

    /**
     * Получить [DatabaseReference] указывающую на путь в базе данных к списку тэгов внутри конкретной задачи users/someUserId/tasks/someTaskId/tags
     *
     * Выбрасывает исключение [AuthentificateException] если пользователь не удалось получить этот путь
     */
    fun getCurrentTaskTagsReference(taskId: String): DatabaseReference {
        return getTasksRootReference().child(taskId).child(TAGS)
    }

    @Deprecated("Dont use this function in production code")
    fun getTestPath(): String {
        return "test"
    }


    @Throws(AuthentificateException::class)
    private fun getTagsRootPath(): String {
        val userID = authRepository.receiveUserId()
        return USERS + SEPARATOR + userID + SEPARATOR + TAGS
    }

    @Throws(AuthentificateException::class)
    private fun getTasksRootPath(): String {
        val userID = authRepository.receiveUserId()
        return USERS + SEPARATOR + userID + SEPARATOR + TASK
    }
}
