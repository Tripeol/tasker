package com.tripeo.tasker.database.dao.writers

import com.google.firebase.database.DatabaseReference
import com.tripeo.tasker.data.models.Tag
import com.tripeo.tasker.data.models.TaskState
import com.tripeo.tasker.database.dao.SubTaskDAO
import com.tripeo.tasker.database.dao.TaskDAO

/**
 * Класс группирующий данные для отправки в firebase DB
 *
 * @param subTaskId идентификатор подзадачи.
 * Если null то при конструированнии данных для отправки на сервер будет создана новая подзадача
 */
class TaskWriter(taskId: String?) {

    var body: String? = null
    private var state: TaskState? = null
    private var subtasks: MutableList<SubtaskWriter>? = null
    private var weight: Int? = null
    private var id = taskId
    private var tags: List<Tag>? = null
    private var tagsToRemove: MutableList<Tag> = mutableListOf()

    /**
     * Установить текст задачи
     *
     * @param body текст задачи
     */
    fun setBody(body: String): TaskWriter {
        this.body = body
        return this
    }

    /**
     * Установить статус задачи
     *
     * @param state статус задачи
     */
    fun setState(state: TaskState): TaskWriter {
        this.state = state
        return this
    }

    /**
     * Установить подзадачи для данной задачи
     *
     * @param subtasks подзадачи для данной задачи
     */
    fun setSubtaskWriters(subtasks: MutableList<SubtaskWriter>?): TaskWriter {
        this.subtasks = subtasks
        return this
    }

    /**
     * Перезаписать данные для единичной подзадачи
     */
    fun addSingleSubtaskWriter(subtask: SubtaskWriter) {
        var localSubtasks = subtasks
        if (localSubtasks != null) {
            val sameSubtask =
                localSubtasks.find { subtaskWriter ->
                    ((subtaskWriter.getId() == subtask.getId())
                        && (subtaskWriter.getId() != null))
                }
            if (sameSubtask != null) {
                if (subtask.getBody() != null) {
                    sameSubtask.setBody(subtask.getBody())
                }
                if (subtask.getState() != null) {
                    sameSubtask.setState(subtask.getState())
                }
            } else {
                localSubtasks.add(subtask)
            }
        } else {
            localSubtasks = ArrayList()
            localSubtasks.add(subtask)
        }
        subtasks = localSubtasks
    }

    /**
     * Установить вес задачи
     *
     * @param weight вес задачи
     */
    fun setWeight(weight: Int?): TaskWriter {
        this.weight = weight
        return this
    }

    /**
     * Установить список тэгов
     */
    fun setTags(tags: List<Tag>) {
        this.tags = tags
    }

    /**
     * Удалить тэг
     */
    fun addSingleTag(tag: Tag) {
        val newTags = tags?.toMutableList() ?: arrayListOf()
        newTags.add(tag)
        this.tags = newTags
    }

    /**
     * Удалить тэг
     */
    fun addTagToRemove(tag: Tag) {
        tagsToRemove.add(tag)
    }

    /**
     * Основной метод данного класса. Он собирает данные в одну [Map] для дальнейшей отправки в БД
     *
     * @param databaseReference ссылка на корень базы данных со списком задач
     * @return [Map] символизирующую json дерево для дальнейшей отправки в БД одним запросом
     */
    fun getMapOfValuesToWrite(databaseReference: DatabaseReference): Map<String, Any?> {
        val mapOfValues = HashMap<String, Any?>()

        var localId: String = id ?: databaseReference.push().key as String

        val localBody = body
        if (localBody != null) {
            mapOfValues.put("$localId${TaskDAO.SEPARATOR}${TaskDAO.TASK_TEXT}", localBody)
        }
        val localState = state
        if (localState != null) {
            mapOfValues.put("$localId${TaskDAO.SEPARATOR}${TaskDAO.TASK_STATE}", localState)
        }
        val localWeight = weight
        if (localWeight != null) {
            mapOfValues.put("$localId${TaskDAO.SEPARATOR}${TaskDAO.TASK_WEIGHT}", localWeight)
        }
        val localSubtasks = subtasks
        if (localSubtasks != null) {
            for (subTaskWriter in localSubtasks) {
                var localSubtaskId = subTaskWriter.getId()
                if (localSubtaskId == null) {
                    val ref = databaseReference.child(localId).child(TaskDAO.SUBTASKS).push()
                    localSubtaskId = ref.key
                }

                val localSubtaskBody = subTaskWriter.getBody()
                if (localSubtaskBody != null) {
                    mapOfValues.put(
                        "$localId${TaskDAO.SEPARATOR}${TaskDAO.SUBTASKS}${TaskDAO.SEPARATOR}$localSubtaskId${TaskDAO.SEPARATOR}${SubTaskDAO.TEXT}",
                        localSubtaskBody
                    )
                }

                val localSubtaskState = subTaskWriter.getState()
                if (localSubtaskState != null) {
                    mapOfValues.put(
                        "$localId${TaskDAO.SEPARATOR}${TaskDAO.SUBTASKS}${TaskDAO.SEPARATOR}$localSubtaskId${TaskDAO.SEPARATOR}${SubTaskDAO.STATE}",
                        localSubtaskState
                    )
                }

                val localSubtaskWeight = subTaskWriter.getWeight()
                if (localSubtaskWeight != null) {
                    mapOfValues.put(
                        "$localId${TaskDAO.SEPARATOR}${TaskDAO.SUBTASKS}${TaskDAO.SEPARATOR}$localSubtaskId${TaskDAO.SEPARATOR}${SubTaskDAO.WEIGHT}",
                        localSubtaskWeight
                    )
                }
            }
        }

        /**
         * Добавляем переданные тэги
         */
        val localTags = tags
        if (localTags != null) {
            for (tag in localTags.filter { tag -> tag.id != null }) {
                mapOfValues["$localId${TaskDAO.SEPARATOR}${TaskDAO.TASK_TAGS}${TaskDAO.SEPARATOR}${tag.id}${TaskDAO.SEPARATOR}${TaskDAO.TAG_NAME}"] =
                    tag.name
            }
        }

        /**
         * Удаляем переданные тэги
         */
        if (tagsToRemove.isNotEmpty()) {
            for (tag in tagsToRemove.filter { tag -> tag.id != null }) {
                mapOfValues["$localId${TaskDAO.SEPARATOR}${TaskDAO.TASK_TAGS}${TaskDAO.SEPARATOR}${tag.id}"] = null
            }
        }
        return mapOfValues
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TaskWriter

        if (body != other.body) return false
        if (state != other.state) return false
        if (subtasks != other.subtasks) return false
        if (weight != other.weight) return false
        if (id != other.id) return false
        if (tags != other.tags) return false

        return true
    }

    override fun hashCode(): Int {
        var result = body?.hashCode() ?: 0
        result = 31 * result + (state?.hashCode() ?: 0)
        result = 31 * result + (subtasks?.hashCode() ?: 0)
        result = 31 * result + (weight ?: 0)
        result = 31 * result + (id?.hashCode() ?: 0)
        result = 31 * result + (tags?.hashCode() ?: 0)
        return result
    }
}