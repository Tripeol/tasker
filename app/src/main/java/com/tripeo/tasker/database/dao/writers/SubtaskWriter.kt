package com.tripeo.tasker.database.dao.writers

import com.tripeo.tasker.data.models.TaskState
import com.tripeo.tasker.database.dao.SubTaskDAO

/**
 * Класс группирующий данные для отправки в firebase DB
 *
 * @param subTaskId идентификатор подзадачи.
 * Если null то при конструированнии данных для отправки на сервер будет создана новая подзадача
 */
class SubtaskWriter(subTaskId: String?) {
    private var body: String? = null
    private var state: TaskState? = null
    private var id: String? = subTaskId
    private var weight: Int? = 0

    /**
     * Установить текст подзадачи
     *
     * @param body текст подзадачи
     */
    fun setBody(body: String?): SubtaskWriter {
        this.body = body
        return this
    }

    /**
     * Установить статус подзадачи
     *
     * @param state статус подзадачи
     */
    fun setState(state: TaskState?): SubtaskWriter {
        this.state = state
        return this
    }

    /**
     * Установить вес подзадачи
     *
     * @param weight вес подзадачи
     */
    fun setWeight(weight: Int?): SubtaskWriter {
        this.weight = weight
        return this
    }

    /**
     * Получить текст подзадачи
     */
    fun getBody(): String? {
        return body
    }

    /**
     * Получить статус подзадачи
     */
    fun getState(): TaskState? {
        return state
    }

    /**
     * получить уникальный id подзадачи
     */
    fun getId(): String? {
        return id
    }

    /**
     * Получить вес подзадачи
     */
    fun getWeight(): Int? {
        return weight
    }
}