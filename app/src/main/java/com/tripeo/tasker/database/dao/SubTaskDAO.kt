package com.tripeo.tasker.database.dao

import com.google.firebase.database.DataSnapshot
import com.tripeo.tasker.data.models.Subtask
import com.tripeo.tasker.data.models.TaskState

/**
 * Класс через который идет общение с БД для сущности [Subtask]
 */
class SubTaskDAO {

    /**
     * Получить список [Subtask] из БД
     * @param dataSnapshot должен указывать на ноду в которой лежит подзадача
     * users/<id юзера>/tasks/<id таски>/subtasks/<id сабтаски>
     */
    fun getSubTask(dataSnapshot: DataSnapshot): Subtask? {
        val body: String? = dataSnapshot.child(TEXT).getValue(String::class.java)
        val state: String? = dataSnapshot.child(STATE).getValue(String::class.java)
        val weight: Int? = dataSnapshot.child(TaskDAO.TASK_WEIGHT).getValue(Int::class.java)
        val id: String? = dataSnapshot.key
        if (state != null) {
            val subtask = Subtask(id, body, TaskState.valueOf(state), weight?:0)
            return subtask
        }
        return null
    }

    companion object {
        val TEXT = "text"
        val STATE = "state"
        val WEIGHT = "weight"
    }
}