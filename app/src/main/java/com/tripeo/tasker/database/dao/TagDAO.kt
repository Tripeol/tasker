package com.tripeo.tasker.database.dao

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseReference
import com.tripeo.tasker.data.models.Tag
import com.tripeo.tasker.database.dao.writers.TagWriter

/**
 * @author Trikhin P O
 * @since 08 11 2020
 */
class TagDAO {

    /**
     * Записать данные по [Tag] в БД
     *
     * @param databaseReference путь в базе данных указывающий на корень со списком тэгов
     * @param writer [Writer] соответствующий [Tag]
     */
    fun writeTag(databaseReference: DatabaseReference, writer: TagWriter) {
        val mapOfValues = writer.getMapOfValuesToWrite(databaseReference)
        databaseReference.updateChildren(mapOfValues)
    }

    /**
     * Получить [Tag] из БД
     * @param dataSnapshot должен указывать на ноду в которой лежит тэг
     * users/<id юзера>/tags/<id нужного тэга>
     */
    fun getTag(dataSnapshot: DataSnapshot): Tag? {
        val name: String? = dataSnapshot.child(TAG_NAME).getValue(String::class.java)
        val id: String? = dataSnapshot.key

        if (name != null) {
            return Tag(id, name)
        }
        return null
    }

    companion object {
        const val TAG_NAME = "name"
    }
}