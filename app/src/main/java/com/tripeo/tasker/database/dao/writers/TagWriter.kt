package com.tripeo.tasker.database.dao.writers

import com.google.firebase.database.DatabaseReference
import com.tripeo.tasker.database.dao.TagDAO
import com.tripeo.tasker.database.dao.TaskDAO

/**
 * Класс группирующий данные сущности [Tag] для отправки в firebase
 *
 * @property tagId идентификатор тэга. Null если создается новый тэг
 *
 * @author Trikhin P O
 * @since 08 11 2020
 */
class TagWriter(val tagId: String?) {

    private var name: String? = null

    /**
     * Установить название тэга
     *
     * @param name текст задачи
     */
    fun setName(name: String): TagWriter {
        this.name = name
        return this
    }

    /**
     * Основной метод данного класса. Он собирает данные в одну [Map] для дальнейшей отправки в БД
     *
     * @param databaseReference ссылка на корень базы данных со списком тэгов
     * @return [Map] символизирующую json дерево для дальнейшей отправки в БД одним запросом
     */
    fun getMapOfValuesToWrite(databaseReference: DatabaseReference): Map<String, Any> {
        val mapOfValues = HashMap<String, Any>()

        var localId: String = tagId ?: databaseReference.push().key as String

        val localName = name
        if (localName != null) {
            mapOfValues.put("$localId${TaskDAO.SEPARATOR}${TagDAO.TAG_NAME}", localName)
        }
        return mapOfValues
    }
}