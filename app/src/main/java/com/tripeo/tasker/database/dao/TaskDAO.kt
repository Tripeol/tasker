package com.tripeo.tasker.database.dao

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseReference
import com.tripeo.tasker.data.models.Subtask
import com.tripeo.tasker.data.models.Tag
import com.tripeo.tasker.data.models.Task
import com.tripeo.tasker.data.models.TaskState
import com.tripeo.tasker.database.dao.writers.TaskWriter

/**
 * Класс через который идет общение с БД для сущности [Task]
 */
class TaskDAO {

    private var subTaskDAO: SubTaskDAO

    init {
        subTaskDAO = SubTaskDAO()
    }

    /**
     * Записать данные по [Task] в БД
     *
     * @param databaseReference путь в базе данных указывающий на корень со списком задач
     * @param writer [Writer] соответствующий [Task]
     */
    fun writeTask(databaseReference: DatabaseReference, writer: TaskWriter) {
        val mapOfValues = writer.getMapOfValuesToWrite(databaseReference)
        databaseReference.updateChildren(mapOfValues)
    }

    /**
     * Получить [Task] из БД
     * @param dataSnapshot должен указывать на ноду в которой лежит задача
     * users/<id юзера>/tasks/<id нужной таски>
     */
    fun getTask(dataSnapshot: DataSnapshot): Task? {
        val body: String? = dataSnapshot.child(TASK_TEXT).getValue(String::class.java)
        val state: String? = dataSnapshot.child(TASK_STATE).getValue(String::class.java)
        val id: String? = dataSnapshot.key
        val weight: Int? = dataSnapshot.child(TASK_WEIGHT).getValue(Int::class.java)

        val subtasks: MutableList<Subtask> = ArrayList()
        for (subtaskDataSnapshot in dataSnapshot.child(SUBTASKS).children) {
            val subTask: Subtask? = subTaskDAO.getSubTask(subtaskDataSnapshot)
            if (subTask != null) {
                subtasks.add(subTask)
            }
        }

        val tags = mutableListOf<Tag>()
        for (tagIdSnapshot in dataSnapshot.child(TASK_TAGS).children) {
            val name = tagIdSnapshot.child(TAG_NAME).getValue(String::class.java)
            if (name != null) {
                tags.add(Tag(tagIdSnapshot.key, name))
            }
        }

        if (state != null) {
            val task = Task(body, TaskState.valueOf(state), id, subtasks, weight, tags)
            return task
        }
        return null
    }

    companion object {
        val TASK_TEXT = "text"
        val TASK_STATE = "state"
        val SUBTASKS = "subtasks"
        val TASK_WEIGHT = "weight"
        val TASK_TAGS = "tags"
        val SEPARATOR = "/"

        val TAG_NAME = "name"
    }
}