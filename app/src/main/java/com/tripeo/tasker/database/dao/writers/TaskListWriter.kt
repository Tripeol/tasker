package com.tripeo.tasker.database.dao.writers

import com.google.android.gms.tasks.Task
import com.tripeo.tasker.database.DatabaseHelper
import javax.inject.Inject

/**
 * Класс группирующий данные для списка [Task] для отправки в firebase DB
 *
 * @author Trikhin P O
 * @since 13 02 2020
 */
class TaskListWriter @Inject constructor(
    val databaseHelper: DatabaseHelper
) {

    private var jsonMap = HashMap<String, Any?>()

    /**
     * Добавить информацию из [TaskWriter] для записи
     *
     * @param taskWriter вспомогательный класс для записи данных по конкретной задаче
     */
    fun addTaskWriter(taskWriter: TaskWriter) {
        jsonMap.putAll(taskWriter.getMapOfValuesToWrite(databaseHelper.getTasksRootReference()))
    }

    /**
     * Получить json дерево по всем записанным задачам
     */
    fun getMapOfValuesToWrite(): Map<String, Any?> {
        return jsonMap
    }
}