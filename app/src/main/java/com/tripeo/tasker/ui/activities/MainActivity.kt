package com.tripeo.tasker.ui.activities

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.lightColorScheme
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.tripeo.tasker.R
import com.tripeo.tasker.auth.AuthRepository
import com.tripeo.tasker.database.DatabaseHelper
import com.tripeo.tasker.database.DatabaseRepository
import com.tripeo.tasker.ui.naviagation.Navigator
import com.tripeo.tasker.ui.screens.Screens
import com.tripeo.tasker.ui.screens.archive.ArchiveScreen
import com.tripeo.tasker.ui.screens.archive.ArchiveViewModel
import com.tripeo.tasker.ui.screens.ceate_tag.CreateTagScreen
import com.tripeo.tasker.ui.screens.ceate_tag.CreateTagViewModel
import com.tripeo.tasker.ui.screens.create_task.CreateTaskScreen
import com.tripeo.tasker.ui.screens.create_task.CreateTaskViewModel
import com.tripeo.tasker.ui.screens.edit_task.EditTaskScreen
import com.tripeo.tasker.ui.screens.edit_task.EditTaskViewModel
import com.tripeo.tasker.ui.screens.sign_in.SignInScreen
import com.tripeo.tasker.ui.screens.sign_in.SignInViewModel
import com.tripeo.tasker.ui.screens.task_list.TaskListScreen
import com.tripeo.tasker.ui.screens.task_list.TaskListViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * Главный экран приложения
 */
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var authRepository: AuthRepository

    @Inject
    lateinit var databaseHelper: DatabaseHelper

    @Inject
    lateinit var databaseRepository: DatabaseRepository

    @Inject
    lateinit var navigator: Navigator


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme(
                colorScheme = lightColorScheme(
                    primary = colorResource(id = R.color.colorPrimary),
                    secondary = colorResource(id = R.color.colorAccent)
                )
            ) {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val navController = rememberNavController()
                    navigator.setupNavController(navController = navController)

                    NavHost(
                        navController = navController,
                        startDestination = Screens.SIGN_IN_SCREEN
                    ) {
                        composable(
                            route = Screens.SIGN_IN_SCREEN
                        ) {
                            val signInViewModel: SignInViewModel by viewModels()
                            SignInScreen(signInViewModel = signInViewModel)
                        }

                        composable(
                            route = Screens.TASK_LIST_SCREEN
                        ) {
                            val taskListViewModel: TaskListViewModel by viewModels()
                            TaskListScreen(taskListViewModel = taskListViewModel)
                        }

                        composable(
                            route = Screens.TAG_CREATE_SCREEN
                        ) {
                            val createTagViewModel: CreateTagViewModel by viewModels()
                            CreateTagScreen(viewModel = createTagViewModel)
                        }

                        composable(
                            route = Screens.TASK_CREATE_SCREEN
                        ) {
                            val createTaskViewModel: CreateTaskViewModel by viewModels()
                            CreateTaskScreen(createTaskViewModel)
                        }

                        composable(
                            route = "${Screens.TASK_EDIT_SCREEN}/{$TASK_ID_KEY}",
                            arguments = listOf(navArgument("$TASK_ID_KEY") {
                                type = NavType.StringType
                            })
                        ) {
                            EditTaskScreen(
                                viewModel = viewModel(
                                    factory = EditTaskViewModel.Companion.Factory(
                                        navigator = navigator,
                                        databaseHelper = databaseHelper,
                                        databaseRepository = databaseRepository,
                                        taskId = requireNotNull(it.arguments?.getString(TASK_ID_KEY))
                                    )
                                ),
                            )
                        }

                        composable(
                            route = Screens.TASK_ARCHIVE
                        ) {
                            val archiveViewModel: ArchiveViewModel by viewModels()
                            ArchiveScreen(archiveViewModel = archiveViewModel)
                        }
                    }
                }
            }
        }
    }

    companion object {
        private const val TASK_ID_KEY = "taskId"
    }
}
