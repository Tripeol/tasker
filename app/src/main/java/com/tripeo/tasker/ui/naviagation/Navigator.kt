package com.tripeo.tasker.ui.naviagation

import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.NavOptionsBuilder
import javax.inject.Inject

interface Navigator {

    fun setupNavController(navController: NavController)

    fun navigate(route: String, builder: NavOptionsBuilder.() -> Unit)

    fun navigate(
        route: String,
        navOptions: NavOptions? = null,
        navigatorExtras: androidx.navigation.Navigator.Extras? = null
    )

    fun popBackStack()

    fun clear()
}

class NavigatorImpl @Inject constructor() : Navigator {

    var navController: NavController? = null
    override fun setupNavController(navController: NavController) {
        this.navController = navController
    }

    override fun navigate(route: String, builder: NavOptionsBuilder.() -> Unit) {
        navController?.navigate(route, builder)
    }

    override fun navigate(
        route: String,
        navOptions: NavOptions?,
        navigatorExtras: androidx.navigation.Navigator.Extras?
    ) {
        navController?.navigate(route, navOptions, navigatorExtras)
    }

    override fun popBackStack() {
        navController?.popBackStack()
    }

    override fun clear() {
        navController = null
    }
}