package com.tripeo.tasker.ui.screens.task_list

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material3.Checkbox
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FilterChip
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.ListItem
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.tripeo.tasker.R
import com.tripeo.tasker.data.models.Tag
import com.tripeo.tasker.data.models.Task
import com.tripeo.tasker.ui.composables.CompositionLogger
import com.tripeo.tasker.ui.screens.ScreenWithAppBar
import com.tripeo.tasker.ui.screens.bottom_navigation.BottomNavigationItem
import com.tripeo.tasker.utils.getFinishedSubtasksAmount


@Composable
fun TaskListScreen(
    taskListViewModel: TaskListViewModel
) {
    val taskListState by taskListViewModel.uiState.collectAsState()
    RenderState(
        taskListState,
        { taskListViewModel.onAddNewTagButtonClicked() },
        { taskListViewModel.onAddNewTaskClicked() },
        { tag: Tag -> taskListViewModel.onTagClicked(tag) },
        { task: Task -> taskListViewModel.onTaskClicked(task) },
        { task: Task -> taskListViewModel.onTaskCompleted(task) },
        { bottomNavigationItem -> taskListViewModel.bottomNavigationClicked(bottomNavigationItem) }
    )
}

@Composable
private fun RenderState(
    state: TaskListState,
    onAddNewTagButtonClicked: () -> Unit,
    onAddNewTaskClicked: () -> Unit,
    onTagClicked: (Tag) -> Unit,
    onTaskClicked: (Task) -> Unit,
    onTaskCompeted: (Task) -> Unit,
    onBottomNavigationClicked: (BottomNavigationItem) -> Unit,
) {
    CompositionLogger.log("TaskListScreen")

    Column {
        ScreenWithAppBar(
            modifier = Modifier.weight(1.0f)
        ) {
            when (state) {
                is TaskListState.Loading -> {
                    RenderLoadingState()
                }

                is TaskListState.Data -> {
                    RenderDataState(
                        state,
                        onAddNewTagButtonClicked,
                        onAddNewTaskClicked,
                        onTagClicked,
                        onTaskClicked,
                        onTaskCompeted
                    )
                }

                is TaskListState.Error -> {
                    RenderErrorState()
                }
            }
        }

        val selectedItem by remember { mutableIntStateOf(0) }
        val items = listOf(
            BottomNavigationItem.TaskList,
            BottomNavigationItem.Archive,
            BottomNavigationItem.Registration
        )
        NavigationBar {
            items.forEachIndexed { index, item ->
                NavigationBarItem(
                    icon = {
                        Icon(
                            imageVector = item.icon,
                            contentDescription = stringResource(id = item.nameRes)
                        )
                    },
                    label = { Text(stringResource(id = item.nameRes)) },
                    selected = selectedItem == index,
                    onClick = {
                        onBottomNavigationClicked(items[index])
                    }
                )
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun RenderDataState(
    state: TaskListState.Data,
    onAddNewTagButtonClicked: () -> Unit,
    onAddNewTaskClicked: () -> Unit,
    onTagClicked: (Tag) -> Unit,
    onTaskClicked: (Task) -> Unit,
    onTaskCompeted: (Task) -> Unit,
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
    ) {
        LazyColumn {
            item {
                ListItem(
                    headlineContent = {
                        Text(
                            text = stringResource(id = R.string.tags_title),
                            fontWeight = FontWeight.Bold
                        )
                    }
                )
            }
            item {
                LazyRow(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.spacedBy(8.dp),
                    contentPadding = PaddingValues(horizontal = 8.dp, vertical = 0.dp)
                ) {
                    if (state.tags != null) {
                        item {
                            FilterChip(
                                selected = true,
                                onClick = onAddNewTagButtonClicked,
                                label = { Text(text = stringResource(id = R.string.create_new_tag)) }
                            )
                        }
                        for (tag in state.tags) {
                            item {
                                FilterChip(
                                    selected = state.selectedTags.contains(tag),
                                    onClick = { onTagClicked(tag) },
                                    label = { Text(text = tag.name) }
                                )
                            }
                        }
                    }
                }
            }
            item {
                ListItem(
                    headlineContent = {
                        Text(
                            text = stringResource(id = R.string.tasks_list),
                            fontWeight = FontWeight.Bold
                        )
                    }
                )
            }
            for (task in state.tasks) {
                item {
                    ListItem(
                        modifier = Modifier.clickable { onTaskClicked(task) },
                        headlineContent = { Text(text = task.body?.trim() ?: "") },
                        supportingContent = {
                            val subtaskAmount = task.subtasks?.size
                            if (subtaskAmount != null) {
                                Text(
                                    text = stringResource(
                                        id = R.string.subtasks_done_pattern,
                                        getFinishedSubtasksAmount(task),
                                        subtaskAmount
                                    )
                                )
                            }
                        },
                        trailingContent = {
                            Checkbox(
                                checked = false,
                                onCheckedChange = { onTaskCompeted(task) })
                        }
                    )
                }
            }
        }
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
                .padding(16.dp)
        ) {
            FloatingActionButton(
                onClick = onAddNewTaskClicked,
                modifier = Modifier.align(Alignment.BottomEnd)
            ) {
                Text(text = stringResource(id = R.string.create_new_task_fab_button))
            }
        }
    }
}

@Composable
private fun RenderLoadingState() {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
    ) {
        CircularProgressIndicator(
            modifier = Modifier
                .align(Alignment.Center),
            color = MaterialTheme.colorScheme.primary,
        )
    }
}

@Composable
private fun RenderErrorState() {
    //TODO: отрисовать состояние с ошибкой
}