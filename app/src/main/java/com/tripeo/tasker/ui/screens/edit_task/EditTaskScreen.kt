package com.tripeo.tasker.ui.screens.edit_task

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FilterChip
import androidx.compose.material3.ListItem
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.tripeo.tasker.R
import com.tripeo.tasker.data.models.TaskState
import com.tripeo.tasker.ui.composables.CompositionLogger
import com.tripeo.tasker.ui.composables.OutlinedTextFieldWithEmptyError
import com.tripeo.tasker.ui.screens.ScreenWithAppBar

@Composable
fun EditTaskScreen(
    viewModel: EditTaskViewModel
) {
    val editTaskState by viewModel.uiState.collectAsState()
    RenderState(
        state = editTaskState,
        onTaskBodyChanged = { body -> viewModel.onTaskBodyChanged(body) },
        onSubtaskChecboxClicked = { id, value -> viewModel.onSubtaskDoneClicked(id, value) },
        onSubtaskCreated = { subtaskBody -> viewModel.onSubtaskCreated(subtaskBody = subtaskBody) },
        onSaveButtonClick = { viewModel.onSaveButtonClick() },
        onTagsBottomSheetTagChecked = { tagId, tagValue ->
            viewModel.onTagBottomSheetTagChecked(
                tagId,
                tagValue
            )
        })
}

@Composable
private fun RenderState(
    state: EditTaskState,
    onTaskBodyChanged: (String) -> Unit,
    onSubtaskChecboxClicked: (String, Boolean) -> Unit,
    onSubtaskCreated: (String) -> Unit,
    onSaveButtonClick: () -> Unit,
    onTagsBottomSheetTagChecked: (String, Boolean) -> Unit
) {
    CompositionLogger.log("EditTaskScreen")
    ScreenWithAppBar {
        when (state.taskState) {
            is EditTaskState.TaskState.Loading -> {
                RenderLoadingState()
            }

            is EditTaskState.TaskState.Data -> {
                RenderDataState(
                    state.taskState,
                    state.tagState,
                    onTaskBodyChanged,
                    onSubtaskChecboxClicked,
                    onSubtaskCreated,
                    onSaveButtonClick,
                    onTagsBottomSheetTagChecked
                )
            }

            is EditTaskState.TaskState.Error -> {
                RenderErrorState()
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun RenderDataState(
    taskState: EditTaskState.TaskState.Data,
    tagState: EditTaskState.TagState,
    onTaskBodyChanged: (String) -> Unit,
    onSubtaskChecboxClicked: (String, Boolean) -> Unit,
    onSubtaskCreated: (String) -> Unit,
    onSaveButtonClick: () -> Unit,
    onTagsBottomSheetTagChecked: (String, Boolean) -> Unit
) {
    var taskName by remember { mutableStateOf(taskState.task.body ?: "") }
    var showCreateSubtaskBottomSheet by remember { mutableStateOf(false) }
    var showAddTagBottomSheet by remember { mutableStateOf(false) }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1.0f)
        ) {

            OutlinedTextFieldWithEmptyError(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 16.dp, top = 8.dp, end = 16.dp),
                value = taskName,
                onValueChange = {
                    taskName = it
                    onTaskBodyChanged(it)
                },
                label = {
                    Text(text = stringResource(id = R.string.create_task_hint))
                },
                supportingTextValue = stringResource(id = R.string.empty_task_text)
            )
            ListItem(
                modifier = Modifier,
                headlineContent = {
                    Text(
                        text = stringResource(id = R.string.tags_title),
                        fontWeight = FontWeight.Bold
                    )
                }
            )
            LazyRow(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 8.dp),
                horizontalArrangement = Arrangement.spacedBy(8.dp),
            ) {
                item {
                    FilterChip(
                        selected = true,
                        onClick = {
                            showAddTagBottomSheet = true
                        },
                        label = { Text(text = stringResource(id = R.string.create_new_tag)) }
                    )
                }
                val taskTags = taskState.task.tags
                if (taskTags != null) {
                    for (tag in taskTags) {
                        item {
                            FilterChip(
                                selected = false,
                                onClick = {
                                    // Nothing to do
                                },
                                label = { Text(text = tag.name) }
                            )
                        }
                    }
                }
            }
            ListItem(
                modifier = Modifier,
                headlineContent = {
                    Text(
                        text = stringResource(id = R.string.subtask_title),
                        fontWeight = FontWeight.Bold
                    )
                }
            )
            LazyColumn(
            ) {
                item {
                    ListItem(
                        headlineContent = {
                            Text(
                                text = stringResource(id = R.string.add_subtask_button),
                            )
                        },
                        trailingContent = {
                            Button(
                                onClick = {
                                    showCreateSubtaskBottomSheet = true
                                },
                            ) {
                                Text(text = stringResource(id = R.string.create_new_subtask_button))
                            }
                        }
                    )
                }
                val subtasks = taskState.task.subtasks?.sortedBy { subtask -> subtask.state }

                if (subtasks != null) {
                    for (subtask in subtasks) {
                        item {
                            ListItem(
                                headlineContent = {
                                    Text(text = subtask.body?.trim() ?: "")
                                },
                                trailingContent = {
                                    Checkbox(
                                        enabled = subtask.state != TaskState.DONE,
                                        checked = subtask.state == TaskState.DONE,
                                        onCheckedChange = {
                                            onSubtaskChecboxClicked(subtask.id ?: "", it)
                                        })
                                }
                            )

                        }
                    }
                }
            }
        }
        Button(
            modifier = Modifier
                .padding(start = 16.dp, end = 16.dp, bottom = 8.dp)
                .fillMaxWidth(),
            onClick = onSaveButtonClick
        ) {
            Text(text = stringResource(id = R.string.save_and_return))
        }
    }

    if (showCreateSubtaskBottomSheet) {
        RenderCreateSubtaskBottomSheet(
            onDismissRequest = {
                showCreateSubtaskBottomSheet = false
            },
            onCreateSubtaskClicked = { subtaskName ->
                showCreateSubtaskBottomSheet = false
                onSubtaskCreated(subtaskName)
            }
        )
    }
    if (showAddTagBottomSheet) {
        RenderChooseTagsBottomSheetDialog(
            taskState = taskState,
            tagState = tagState,
            onTagChecked = onTagsBottomSheetTagChecked,
            onDismissRequest = { showAddTagBottomSheet = false }
        )
    }

}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun RenderCreateSubtaskBottomSheet(
    onDismissRequest: () -> Unit,
    onCreateSubtaskClicked: (String) -> Unit,
) {
    ModalBottomSheet(onDismissRequest = onDismissRequest) {
        var subtaskName by remember { mutableStateOf("") }

        LazyColumn {
            item {
                ListItem(headlineContent = {
                    Text(
                        text = stringResource(id = R.string.create_subtask_title),
                        fontWeight = FontWeight.Bold
                    )
                })
            }
            item {
                OutlinedTextFieldWithEmptyError(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp),
                    value = subtaskName,
                    onValueChange = {
                        subtaskName = it
                    },
                    label = {
                        Text(text = stringResource(id = R.string.create_subtask_hint))
                    },
                    supportingTextValue = stringResource(id = R.string.empty_task_text)
                )
            }
        }
        Button(
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 16.dp, end = 16.dp, bottom = 16.dp),
            onClick = {
                if (subtaskName.isNotBlank()) {
                    onCreateSubtaskClicked(subtaskName)
                }
            },
            shape = ButtonDefaults.filledTonalShape
        ) {
            Text(text = stringResource(id = R.string.create_subtask_button))
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun RenderChooseTagsBottomSheetDialog(
    taskState: EditTaskState.TaskState,
    tagState: EditTaskState.TagState,
    onTagChecked: (String, Boolean) -> Unit,
    onDismissRequest: () -> Unit,
) {
    ModalBottomSheet(onDismissRequest = onDismissRequest) {
        if (tagState is EditTaskState.TagState.Data && taskState is EditTaskState.TaskState.Data) {
            Column {
                ListItem(
                    headlineContent = {
                        Text(
                            text = stringResource(id = R.string.choose_tags),
                            fontWeight = FontWeight.Bold
                        )
                    }
                )
                LazyColumn {
                    for (tag in tagState.tags) {
                        item {
                            ListItem(headlineContent = {
                                Text(text = tag.name)
                            },
                                trailingContent = {
                                    Checkbox(
                                        checked = (taskState.task.tags ?: listOf()).contains(tag),
                                        onCheckedChange = {
                                            tag.id?.let { tagId -> onTagChecked(tagId, it) }
                                        }
                                    )
                                })
                        }
                    }
                }
            }
        } else if (tagState is EditTaskState.TagState.Error || taskState is EditTaskState.TaskState.Error) {
            // TODO обработать состояние ошибки
        } else {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(240.dp)
            ) {
                CircularProgressIndicator(
                    modifier = Modifier
                        .align(Alignment.Center),
                    color = MaterialTheme.colorScheme.primary,
                )
            }
        }
    }
}

@Composable
private fun RenderLoadingState() {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
    ) {
        CircularProgressIndicator(
            modifier = Modifier
                .align(Alignment.Center),
            color = MaterialTheme.colorScheme.primary,
        )
    }
}

@Composable
private fun RenderErrorState() {
    //TODO обработать состояние ошибки
}