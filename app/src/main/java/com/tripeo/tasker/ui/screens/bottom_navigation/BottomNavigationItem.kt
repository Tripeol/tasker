package com.tripeo.tasker.ui.screens.bottom_navigation

import androidx.annotation.StringRes
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.ExitToApp
import androidx.compose.material.icons.filled.List
import androidx.compose.ui.graphics.vector.ImageVector
import com.tripeo.tasker.R
import com.tripeo.tasker.ui.screens.Screens

/**
 * Класс содержащий данные для BottomNavigation item'ов
 */
sealed class BottomNavigationItem(
    @StringRes val nameRes: Int,
    val icon: ImageVector,
    val navigationRoute: String
) {

    object TaskList :
        BottomNavigationItem(R.string.task_list, Icons.Filled.List, Screens.TASK_ARCHIVE)

    object Archive :
        BottomNavigationItem(R.string.archive, Icons.Filled.Delete, Screens.TASK_ARCHIVE)

    object Registration : BottomNavigationItem(R.string.logout, Icons.Filled.ExitToApp, Screens.SIGN_IN_SCREEN)

}