package com.tripeo.tasker.ui.screens.ceate_tag

import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.tripeo.tasker.data.models.Tag
import com.tripeo.tasker.database.DatabaseHelper
import com.tripeo.tasker.database.DatabaseRepository
import com.tripeo.tasker.database.dao.writers.TagWriter
import com.tripeo.tasker.ui.naviagation.Navigator
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CreateTagViewModel @Inject constructor(
    val navigator: Navigator,
    val databaseHelper: DatabaseHelper,
    val databaseRepository: DatabaseRepository
) : ViewModel() {

    private var tags: List<Tag> = listOf()

    init {
        /**
         * Подписка на изменения в списке тэгов
         */
        databaseHelper.getTagsRootReference()
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    tags = databaseRepository.getTags(dataSnapshot)
                }

                override fun onCancelled(p0: DatabaseError) {
                }
            })
    }


    /**
     * Пользователь нажал на кнопку "Создать тэг"
     */
    fun onCreateTagClicked(tagName: String) {
        if (tags.find { tag -> tag.name.trim() == tagName.trim() } != null) {
            //TODO обработать название тега с таким же именем
        } else {
            val tagWriter: TagWriter = TagWriter(null)
                .setName(tagName)
            databaseRepository.writeTagWithWriter(tagWriter)
            navigator.popBackStack()
        }
    }
}