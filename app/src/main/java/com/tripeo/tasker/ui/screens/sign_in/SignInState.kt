package com.tripeo.tasker.ui.screens.sign_in

/**
 * Состояние экрана [SignInScreen]
 *
 * @property checkAuth флаг отвечающия за то необходимо ли запустить процесс регистрации
 */
data class SignInState(val checkAuth: Boolean) {
    companion object {
        val INITIAL: SignInState = SignInState(false)
    }
}


