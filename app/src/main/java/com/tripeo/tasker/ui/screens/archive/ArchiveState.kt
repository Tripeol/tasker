package com.tripeo.tasker.ui.screens.archive

import com.tripeo.tasker.data.models.Task


/**
 * Состояние экрана с завершенными задачами
 */
sealed class ArchiveState {

    /**
     * Состояние с данными
     */
    data class Data(val tasks: List<Task>) : ArchiveState()

    /**
     * Состояние загрузки
     */
    object Loading : ArchiveState()

    /**
     * Состояние ошибки
     */
    object Error : ArchiveState()

    companion object {
        val INITIAL: ArchiveState = Loading
    }
}