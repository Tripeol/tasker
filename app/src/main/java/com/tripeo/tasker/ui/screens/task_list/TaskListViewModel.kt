package com.tripeo.tasker.ui.screens.task_list

import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.tripeo.tasker.data.models.Tag
import com.tripeo.tasker.data.models.Task
import com.tripeo.tasker.data.models.TaskState
import com.tripeo.tasker.database.DatabaseHelper
import com.tripeo.tasker.database.DatabaseRepository
import com.tripeo.tasker.database.dao.writers.TaskWriter
import com.tripeo.tasker.ui.naviagation.Navigator
import com.tripeo.tasker.ui.screens.Screens
import com.tripeo.tasker.ui.screens.bottom_navigation.BottomNavigationItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject

@HiltViewModel
class TaskListViewModel @Inject constructor(
    val navigator: Navigator,
    val databaseHelper: DatabaseHelper,
    val databaseRepository: DatabaseRepository,
) : ViewModel() {

    private val _uiState = MutableStateFlow(TaskListState.INITIAL)
    val uiState: MutableStateFlow<TaskListState> = _uiState

    private var allNotCompletedTasks: List<Task> = listOf()

    init {
        /**
         * Подписка на изменения в списке задач
         */
        databaseHelper.getTasksRootReference()
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    allNotCompletedTasks =
                        (databaseRepository.getTasks(dataSnapshot)
                            .filter { it.state != TaskState.DONE }).sortedByDescending { task -> task.weight }
                    uiState.update { state ->
                        if (state is TaskListState.Data) {
                            state.copy(tasks = allNotCompletedTasks)
                        } else {
                            TaskListState.Data(tasks = allNotCompletedTasks)
                        }
                    }
                }

                override fun onCancelled(p0: DatabaseError) {
                    //TODO Обработать ошибку загрузки задач
                }
            })

        /**
         * Подписка на изменения в списке тэгов
         */
        databaseHelper.getTagsRootReference()
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    uiState.update { state ->
                        val newTags = databaseRepository.getTags(dataSnapshot).toMutableList()
                        if (state is TaskListState.Data) {
                            state.copy(tags = newTags)
                        } else {
                            TaskListState.Data(listOf(), newTags)
                        }
                    }
                }

                override fun onCancelled(p0: DatabaseError) {
                    //TODO Обработать ошибку загрузки тэгов
                }
            })
    }

    /**
     * Пользователь нажал на кнопку добавления нового тэга
     */
    fun onAddNewTagButtonClicked() {
        navigator.navigate(Screens.TAG_CREATE_SCREEN)
    }

    fun onAddNewTaskClicked() {
        navigator.navigate(Screens.TASK_CREATE_SCREEN)
    }

    /**
     * Пользователь нажал на тэг в списке тэгов
     */
    fun onTagClicked(tag: Tag) {
        uiState.update { state ->
            if (state is TaskListState.Data) {
                val currentSelectedTags = state.selectedTags.toMutableSet()
                if (currentSelectedTags.contains(tag)) {
                    currentSelectedTags.remove(tag)
                } else {
                    currentSelectedTags.add(tag)
                }
                state.copy(
                    selectedTags = currentSelectedTags,
                    tasks = gitTaskFilteredByTags(currentSelectedTags)
                )
            } else {
                TaskListState.Error
            }
        }
    }

    /**
     * Пользователь нажал на одну из задач в списке
     */
    fun onTaskClicked(task: Task) {
        val taskId = task.id
        if (taskId != null) {
            navigator.navigate(Screens.TASK_EDIT_SCREEN + "/$taskId")
        }
    }

    /**
     * Пользователь пометил задачу как завершенную (нажав на чекбокс)
     */
    fun onTaskCompleted(task: Task) {
        val taskWriter = TaskWriter(task.id)
        taskWriter.setState(TaskState.DONE)
        databaseRepository.writeTaskWithWriter(taskWriter)
    }

    fun bottomNavigationClicked(bottomNavigationItem: BottomNavigationItem) {
        navigator.navigate(bottomNavigationItem.navigationRoute)
    }

    private fun gitTaskFilteredByTags(selectedTags: Set<Tag>?): List<Task> {
        if (!selectedTags.isNullOrEmpty()) {
            return allNotCompletedTasks.filter { task ->
                (task.tags ?: listOf()).containsAll(selectedTags)
            }
        } else {
            return allNotCompletedTasks
        }
    }
}