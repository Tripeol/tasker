package com.tripeo.tasker.ui.screens.sign_in

import androidx.lifecycle.ViewModel
import com.tripeo.tasker.auth.AuthRepository
import com.tripeo.tasker.auth.AuthentificateException
import com.tripeo.tasker.ui.naviagation.Navigator
import com.tripeo.tasker.ui.screens.Screens
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject

@HiltViewModel
class SignInViewModel @Inject constructor(
    val navigator: Navigator,
    val authRepository: AuthRepository,
) : ViewModel() {

    private val _uiState = MutableStateFlow(SignInState.INITIAL)
    val uiState: MutableStateFlow<SignInState> = _uiState

    init {
        checkAuth()
    }

    /**
     * Пользователь нажал на кнопку "Регистрация"
     */
    fun onRegistrationButtonClicked() {
        checkAuth()
    }

    /**
     * Пользователь успешно авторизовался в приложении
     */
    fun onRegistrationCompleted() {
        navigateToTaskListScreen()
    }

    private fun checkAuth() {
        try {
            authRepository.receiveUserId()
            navigateToTaskListScreen()
        } catch (e: AuthentificateException) {
            uiState.update { state -> state.copy(checkAuth = true) }
        }
    }

    private fun navigateToTaskListScreen() {
        navigator.navigate(Screens.TASK_LIST_SCREEN) {
            popUpTo(Screens.SIGN_IN_SCREEN) {
                inclusive = true
            }
        }
    }
}