package com.tripeo.tasker.ui.screens.edit_task

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.tripeo.tasker.data.models.Task
import com.tripeo.tasker.data.models.TaskState
import com.tripeo.tasker.database.DatabaseHelper
import com.tripeo.tasker.database.DatabaseRepository
import com.tripeo.tasker.database.dao.writers.SubtaskWriter
import com.tripeo.tasker.database.dao.writers.TaskWriter
import com.tripeo.tasker.ui.naviagation.Navigator
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update

class EditTaskViewModel(
    val navigator: Navigator,
    val databaseHelper: DatabaseHelper,
    val databaseRepository: DatabaseRepository,
    val taskId: String,
) : ViewModel() {

    private val _uiState = MutableStateFlow(
        EditTaskState(
            taskState = EditTaskState.TaskState.Loading,
            tagState = EditTaskState.TagState.Loading
        )
    )
    val uiState: MutableStateFlow<EditTaskState> = _uiState

    private var taskWriter: TaskWriter

    init {
        taskWriter = TaskWriter(taskId)


        /**
         * Подписка на изменения в данных задачи
         */
        databaseHelper.getTasksRootReference().child(taskId)
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val newTask: Task? = databaseRepository.getTask(dataSnapshot)
                    if (newTask != null) {
                        uiState.update { state ->
                            state.copy(taskState = EditTaskState.TaskState.Data(newTask))
                        }
                    }
                }

                override fun onCancelled(p0: DatabaseError) {
                }
            })

        /**
         * Подписка на изменения в списке тэгов
         */
        databaseHelper.getTagsRootReference()
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val userTags = databaseRepository.getTags(dataSnapshot)
                    uiState.update { state ->
                        state.copy(tagState = EditTaskState.TagState.Data(userTags))
                    }
                }

                override fun onCancelled(p0: DatabaseError) {
                }
            })
    }

    /**
     * Пользователь нажал на галочку напротив сабтаски.
     */
    fun onSubtaskDoneClicked(subtaskId: String, checkboxValue: Boolean) {
        if (checkboxValue) {
            val subtaskWriter = SubtaskWriter(subtaskId)
            subtaskWriter.setState(TaskState.DONE)
            taskWriter.addSingleSubtaskWriter(subtaskWriter)
            writeTaskWithCurrentWriter()
        }
    }

    /**
     * Пользователь изменил название таски
     */
    fun onTaskBodyChanged(body: String) {
        taskWriter.setBody(body)
    }

    /**
     * Пользователь нажал на кнопку создания подзадачи внутри диалога создания подзадачи.
     */
    fun onSubtaskCreated(subtaskBody: String) {
        if (subtaskBody.isNotEmpty()) {
            val subtaskWriter: SubtaskWriter =
                SubtaskWriter(null).setBody(subtaskBody).setState(TaskState.TODO)
            taskWriter.addSingleSubtaskWriter(subtaskWriter)
            writeTaskWithCurrentWriter()
        }
    }

    /**
     * Пользователь нажал на кнопку сохранения изменений
     */
    fun onSaveButtonClick() {
        val body = taskWriter.body
        if (body == null || body.isNotEmpty()) {
            writeTaskWithCurrentWriter()
            navigator.popBackStack()
        }
    }

    fun onTagBottomSheetTagChecked(tagId: String, tagValue: Boolean) {
        uiState.update { state ->
            val taskState = state.taskState
            val tagState = state.tagState
            if (taskState is EditTaskState.TaskState.Data && tagState is EditTaskState.TagState.Data) {
                val newTags = (taskState.task.tags ?: arrayListOf()).toMutableList()
                val targetTag = tagState.tags.firstOrNull { tag -> tag.id == tagId }
                if (tagValue) {
                    targetTag?.let {
                        newTags.add(it)
                        taskWriter.addSingleTag(it)
                        writeTaskWithCurrentWriter()
                    }
                } else {
                    targetTag?.let {
                        newTags.remove(it)
                        taskWriter.addTagToRemove(it)
                        writeTaskWithCurrentWriter()
                    }
                }
                state.copy(
                    taskState = taskState.copy(taskState.task.copy(tags = newTags))
                )
            } else {
                state
            }
        }
    }

    private fun writeTaskWithCurrentWriter() {
        databaseRepository.writeTaskWithWriter(taskWriter)
        taskWriter = TaskWriter(taskId)
    }

    companion object {
        class Factory(
            val navigator: Navigator,
            val databaseHelper: DatabaseHelper,
            val databaseRepository: DatabaseRepository,
            val taskId: String,
        ) : ViewModelProvider.NewInstanceFactory() {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return EditTaskViewModel(
                    navigator = navigator,
                    databaseHelper = databaseHelper,
                    databaseRepository = databaseRepository,
                    taskId = taskId
                ) as T
            }
        }

    }
}