package com.tripeo.tasker.ui.composables

import android.util.Log

object CompositionLogger {

    fun log(message: String) {
        Log.d(COMPOSITION_TAG, message)
    }

    private const val COMPOSITION_TAG = "composition"

}