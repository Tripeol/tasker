package com.tripeo.tasker.ui.screens.create_task

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.tripeo.tasker.R
import com.tripeo.tasker.ui.composables.OutlinedTextFieldWithEmptyError

@Composable
fun CreateTaskScreen(
    viewModel: CreateTaskViewModel
) {
    RenderState(
        { string -> viewModel.onCreateTaskClicked(string) }
    )
}

@Composable
private fun RenderState(
    onCreateTaskClicked: (String) -> Unit
) {

    Column(
        verticalArrangement = Arrangement.SpaceBetween
    ) {

        var taskName by remember { mutableStateOf("") }

        LazyColumn(
            contentPadding = PaddingValues(start = 16.dp, end = 16.dp)
        ) {
            item {
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 16.dp),
                    text = stringResource(id = R.string.create_task_title),
                    textAlign = TextAlign.Center,
                )
            }
            item {
                OutlinedTextFieldWithEmptyError(
                    modifier = Modifier.fillMaxWidth(),
                    value = taskName,
                    onValueChange = {
                        taskName = it
                    },
                    label = {
                        Text(text = stringResource(id = R.string.create_task_hint))
                    },
                    supportingTextValue = stringResource(id = R.string.empty_task_text)
                )
            }
        }
        Button(
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 16.dp, end = 16.dp, bottom = 16.dp),
            onClick = {
                if (taskName.isNotBlank()) {
                    onCreateTaskClicked(taskName)
                }
            },
            shape = ButtonDefaults.filledTonalShape
        ) {
            Text(text = stringResource(id = R.string.create_task_button))
        }
    }
}