package com.tripeo.tasker.ui.screens.archive

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ListItem
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextDecoration
import com.tripeo.tasker.R
import com.tripeo.tasker.utils.getFinishedSubtasksAmount

@Composable
fun ArchiveScreen(
    archiveViewModel: ArchiveViewModel
) {
    val archiveState by archiveViewModel.uiState.collectAsState()
    RenderState(archiveState = archiveState)
}

@Composable
private fun RenderState(
    archiveState: ArchiveState
) {
    when (archiveState) {
        is ArchiveState.Loading -> {
            RenderLoadingState()
        }

        is ArchiveState.Data -> {
            RenderDataState(
                archiveState
            )
        }

        is ArchiveState.Error -> {
            RenderErrorState()
        }
    }
}

@Composable
private fun RenderLoadingState() {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
    ) {
        CircularProgressIndicator(
            modifier = Modifier
                .align(Alignment.Center),
            color = MaterialTheme.colorScheme.primary,
        )
    }
}

@Composable
private fun RenderDataState(
    state: ArchiveState.Data
) {
    LazyColumn {
        for (task in state.tasks) {
            item {
                ListItem(
                    headlineContent = {
                        Text(
                            text = task.body?.trim() ?: "",
                            textDecoration = TextDecoration.LineThrough
                        )
                    },
                    supportingContent = {
                        val subtaskAmount = task.subtasks?.size
                        if (subtaskAmount != null) {
                            Text(
                                text = stringResource(
                                    id = R.string.subtasks_done_pattern,
                                    getFinishedSubtasksAmount(task),
                                    subtaskAmount
                                )
                            )
                        }
                    }
                )
            }
        }
    }
}

@Composable
private fun RenderErrorState() {
    //TODO: отрисовать состояние с ошибкой
}