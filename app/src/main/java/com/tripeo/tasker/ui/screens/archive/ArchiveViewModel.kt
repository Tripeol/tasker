package com.tripeo.tasker.ui.screens.archive

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.tripeo.tasker.data.models.TaskState
import com.tripeo.tasker.database.DatabaseHelper
import com.tripeo.tasker.database.DatabaseRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject

@HiltViewModel
class ArchiveViewModel @Inject constructor(
    databaseHelper: DatabaseHelper,
    databaseRepository: DatabaseRepository,
) : ViewModel() {

    private val _uiState = MutableStateFlow(ArchiveState.INITIAL)
    val uiState: MutableStateFlow<ArchiveState> = _uiState

    init {
        /**
         * Подписка на изменения в списке задач
         */
        databaseHelper.getTasksRootReference()
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val allCompletedTasks =
                        (databaseRepository.getTasks(dataSnapshot)
                            .filter { it.state == TaskState.DONE }).sortedByDescending { task -> task.weight }
                    uiState.update { state ->
                        if (state is ArchiveState.Data) {
                            state.copy(tasks = allCompletedTasks)
                        } else {
                            ArchiveState.Data(tasks = allCompletedTasks)
                        }
                    }
                }

                override fun onCancelled(p0: DatabaseError) {
                    //TODO Обработать ошибку загрузки задач
                }
            })
    }


    companion object {
        class Factory(
            val databaseHelper: DatabaseHelper,
            val databaseRepository: DatabaseRepository
        ) : ViewModelProvider.NewInstanceFactory() {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return ArchiveViewModel(
                    databaseHelper = databaseHelper,
                    databaseRepository = databaseRepository
                ) as T
            }
        }

    }

}