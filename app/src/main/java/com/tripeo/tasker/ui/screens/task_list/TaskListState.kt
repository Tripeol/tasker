package com.tripeo.tasker.ui.screens.task_list

import com.tripeo.tasker.data.models.Tag
import com.tripeo.tasker.data.models.Task

/**
 * Состояния экрана со списком задач
 */
sealed class TaskListState() {

    /**
     * Состояние экрана с данными
     *
     * @property tasks список задач
     * @property tags список тэгов
     */
    data class Data(
        val tasks: List<Task>,
        val tags: List<Tag>? = null,
        val selectedTags: Set<Tag> = hashSetOf()
    ) : TaskListState()

    /**
     * Состояние экрана в момент загрузки данных
     */
    object Loading : TaskListState()

    /**
     * Состояние экрана с ошибкой
     */
    object Error : TaskListState()

    companion object {
        val INITIAL: TaskListState = Loading
    }
}