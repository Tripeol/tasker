package com.tripeo.tasker.ui.screens.edit_task

import com.tripeo.tasker.data.models.Tag
import com.tripeo.tasker.data.models.Task

/**
 * Состояние экрана редактирования задачи.
 */
data class EditTaskState(
    val taskState: TaskState,
    val tagState: TagState,
) {

    sealed class TaskState() {
        /**
         * Состояние с данными
         */
        data class Data(
            val task: Task,
        ) : TaskState()

        /**
         * Состояние с ошибкой
         */
        object Error : TaskState()

        /**
         * Состояние загрузки
         */
        object Loading : TaskState()
    }


    /**
     * Состояние bottomSheet с тэгами
     */
    sealed class TagState() {

        /**
         * Состояние когда тэги прогрузились
         */
        data class Data(val tags: List<Tag>) : TagState()

        /**
         * Состояние загрузки тэгов
         */
        object Loading : TagState()

        /**
         * Состояние ошибки загрузки тэгов
         */
        object Error : TagState()
    }
}