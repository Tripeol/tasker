package com.tripeo.tasker.ui.screens.sign_in

import android.app.Activity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Box
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.firebase.ui.auth.AuthUI
import com.tripeo.tasker.R

@Composable
fun SignInScreen(
    signInViewModel: SignInViewModel,
) {
    val signInState by signInViewModel.uiState.collectAsState()
    RenderState(
        state = signInState,
        onRegistrationSuccess = { signInViewModel.onRegistrationCompleted() },
        onRegistrationButtonClicked = { signInViewModel.onRegistrationButtonClicked() }
    )
}

@Composable
private fun RenderState(
    state: SignInState,
    onRegistrationSuccess: () -> Unit,
    onRegistrationButtonClicked: () -> Unit
) {
    val launcher =
        rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                onRegistrationSuccess.invoke()
            }
        }

    Box {
        Button(
            modifier = Modifier.align(Alignment.Center),
            onClick = onRegistrationButtonClicked,
        ) {
            Text(text = stringResource(id = R.string.sign_in_button))
        }
    }
    if (state.checkAuth) {
        val providers = arrayListOf(
            AuthUI.IdpConfig.GoogleBuilder().build()
        )
        SideEffect {
            launcher.launch(
                AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .setAvailableProviders(providers)
                    .build(),
            )
        }
    }
}