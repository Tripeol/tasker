package com.tripeo.tasker.ui.screens.create_task

import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import com.tripeo.tasker.data.models.TaskState
import com.tripeo.tasker.database.DatabaseRepository
import com.tripeo.tasker.database.dao.writers.TaskWriter
import com.tripeo.tasker.ui.naviagation.Navigator
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CreateTaskViewModel @Inject constructor(
    val navigator: Navigator,
    val databaseRepository: DatabaseRepository
) : ViewModel() {

    /**
     * Пользователь нажал на кнопку "Создать задачу"
     */
    fun onCreateTaskClicked(taskName: String) {
        if (taskName.isNotEmpty()) {
            val taskWriter: TaskWriter =
                TaskWriter(null).setBody(taskName).setState(TaskState.TODO).setWeight(0)
            databaseRepository.writeTaskWithWriter(taskWriter)
            navigator.popBackStack()
        }
    }
}