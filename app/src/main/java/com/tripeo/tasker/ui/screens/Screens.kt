package com.tripeo.tasker.ui.screens

object Screens {

    private const val DELIMETER = "/"

    const val SIGN_IN_SCREEN = "sign_in"
    const val TASK_LIST_SCREEN = "/task_list"
    const val TAG_CREATE_SCREEN = TASK_LIST_SCREEN + DELIMETER + "tag_create"
    const val TASK_CREATE_SCREEN = TASK_LIST_SCREEN + DELIMETER + "task_create"
    const val TASK_EDIT_SCREEN = TASK_LIST_SCREEN + DELIMETER + "task_edit"
    const val TASK_ARCHIVE = "/archive"

}